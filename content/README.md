[toc]

# Dokumentation mit Hugo

Beschreibt, wie ein in GitLab liegende Dokumentation per Hugo als Seite in gitLab zur Verfügung gestellt werden kann.

Hugo wird lokal verwendet, um die Seite zu testen. Im GitLab wird die Generierung per Pipeline bei Änderungen am Markdown  automatisch durchgeführt.

Das Repository beinhaltet sowohl das Generierungszug von Hugo also auch die Dokumentation selbst.

## MacBook Setup

Repository auschecken nach '~/dev/docs'.

```
% cd ~/dev
% git clone https://gitlab.com/christian-schulzendorff/docs.git
% cd docs
```

Hugo wird im Docker-Container verwendet. Es wird vorausgesetzt, dass Docker Desktop installiert ist. Folgendes Image von https://hub.docker.com/r/klakegg/hugo  wird verwendet

    0.101.0-ext-asciidoctor

## GitLab konfigurieren

S. https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/#push-your-hugo-website-to-gitlab

## Änderungen durchführen

Änderungen sowohl an der Dokumentation als auch an dem Hugo-Projekt können lokal durchgeführt und getestet werden. Alternativ wird direkt im GitLab geändert, kann dann aber nicht getestet werden.

Der Dokumentationsinhalt liegt im Unterverzeichnis _content_. Hugo generiert in Verzeichnis _public_, was auch von GitLab erwartet wird.

### Änderung lokal durchführen

Seiten neu generieren und lokalen Server starten:

```
% docker run --rm -it -v $(pwd):/src klakegg/hugo:0.101.0-ext-asciidoctor
% docker run --rm -it -v $(pwd):/src -p 1313:1313 klakegg/hugo:0.101.0-ext-asciidoctor server -D
```


### GitLab

Die URL in der Konfiguration _config.toml_ anpassen:
```
baseURL = 'https://gitlab.com/christian-schulzendorff/docs/'
```

In den Repro-Einstellungen _Pages_ einschalten und für jedermann sichtbar machen unter _Settings > General > Visability.



## Repository aufsetzen

Beschreibt, wie das leere GitLab-Repository initial aufgesetzt wird. Das MacBook Setup ist bereits erfolgt.

Verzeichnis für die Dokumentation erstellen und dort diese MD-Datei anlegen:
```
% cd ~/dev/docs
% touch README.md
```
Neues Hugo-Projekt erzeugen und im Hauptverzeichnis ablegen:

```
% docker run --rm -it -v $(pwd):/src klakegg/hugo:0.101.0 new site newsite
%  mv newsite/* .
```
Theme 'Hugo Book' installieren:
 
```
% git submodule add https://github.com/alex-shpak/hugo-book themes/hugo-book
% cp -R themes/hugo-book/exampleSite/content.en/* ./content
```

Das Theme in config.toml bekannt geben:

```
theme = 'hugo-book'
```


